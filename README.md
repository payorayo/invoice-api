# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

I created a API for the challenge because it's flexible and reusable for other apps. Create unit test is easy because the code is separated in controllers and provider. The architecture allows to update and add without problems.

The API includes controllers and unit test of invoices, clients, users, payment method, article and auth. But in the frontend only I did Invoices and login modules. Because my idea was create all modules but I would take more time.

I use to React for front-end side because I like react.js and I wanted to use it in the challenge.
* Version
    PHP > 7.0

### How do I get set up? ###

* Summary of set up.

You need to download the repository. After create a database, modify the config script and run the migration script in the root. 
        
* Configuration

follow the steps below:

    1. Create your database.
    2. Write your credential in **config/core.php**
    3. Modify the variable **FOLDER_ROOT** with the path where the project is
    4. Run the script **/dbseed.php**. This script create all tables and add records in each table

* Dependencies

    1. You need to run composer install

* How to run tests

    1. You need to run ./vendor/bin/phpunit **PATH_PROJECT**\tests

    
### Guidelines ###

* [Api Documentation](https://documenter.getpostman.com/view/1534207/SztHWk11?version=latest#baa1dae3-5c72-4083-b65d-c40b8a09684d)  
### Summary ###

* Limitations
    
    The project don't use a token (JWT)  for add a security layer. Also There are limitations in the design of databases as I do not include a role table and I do not include key primary as rfc. 
    
* PROS
    
    1. The project can consume for other app in different tecnology (mobile or web) or language (c#, java)
    2. The Api is easy to use because It has documentation
    3. The code is easy to read because the code is separated in controllers and providers.
    4. The code is easy to test 
    
* CONS
    1. There are limitations with the routes. I created a custom middleware to match from url to controller.
    2. It's slower create API than a unique project that combine PHP classes with frontend.

    