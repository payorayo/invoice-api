<?php
require './bootstrap.php';

$stmArticle = <<<EOS
    CREATE TABLE IF NOT EXISTS articles (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(250) NOT NULL,
        description TINYTEXT,
        amount INT(5) NOT NULL,
        price FLOAT NOT NULL,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_DATE(),
        updated_at TIMESTAMP NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO articles
        (id, name, description, amount, price, created_at, updated_at)
    VALUES
        (1, 'Tennis Nike', '', 15, 120.55, NOW(), null),
        (2, 'Jacket', '', 20, 1000.45, NOW(), null),
        (3, 'Watch', '', 50, 320.13, NOW(), null),
        (4, 'Car', '', 15, 50000.00, NOW(), null),
        (5, 'Dress', '', 45, 40.00, NOW(), null),
        (6, 'Shoes', '', 50, 100.99, NOW(), null),
        (7, 'Laptop', '', 25, 11000.00, NOW(), null),
        (8, 'Mouse', '', 34, 351.45, NOW(), null),
        (9, 'Glasses', '', 34, 8.55, NOW(), null);
EOS;

$stmClient = <<<EOS
    CREATE TABLE IF NOT EXISTS clients (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(250) NOT NULL,
        rfc VARCHAR(20) NOT NULL,
        email VARCHAR(250) NOT NULL,
        address VARCHAR(250) NOT NULL,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_DATE(),
        updated_at TIMESTAMP NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO clients
        (id, name, rfc, email, address, created_at, updated_at)
    VALUES
        (1, 'Baja_Start SA de CV', 'XXXX010101X12', 'exemple@domain.com','', NOW(), null),
        (2, 'Juan Perez', 'XXXX010101X12', 'exemple@domain.com', '', NOW(), null),
        (3, 'Toyota MX', 'XXXX010101X12', 'exemple@domain.com', '', NOW(), null),
        (4, 'Fernando Martinez', 'XXXX010101X12', 'exemple@domain.com', '', NOW(), null);
EOS;

$stmPayment = <<<EOS
    CREATE TABLE IF NOT EXISTS payment_methods (
        id INT NOT NULL AUTO_INCREMENT,
        name VARCHAR(250) NOT NULL,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_DATE(),
        updated_at TIMESTAMP NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO payment_methods
        (id, name, created_at, updated_at)
    VALUES
        (1, 'Efectivo', NOW(), null),
        (2, 'Tarjeta de Credito', NOW(), null),
        (3, 'Cheque', NOW(), null);
EOS;

$stmUsers = <<<EOS
    CREATE TABLE IF NOT EXISTS users (
        id INT NOT NULL AUTO_INCREMENT,
        email VARCHAR(250) NOT NULL,
        password VARCHAR(250) NOT NULL,
        first_name VARCHAR(250) NOT NULL,
        last_name VARCHAR(250) NOT NULL,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_DATE(),
        updated_at TIMESTAMP NULL,
        PRIMARY KEY (id)
    ) ENGINE=INNODB;

    INSERT INTO users
        (id, email, password, first_name, last_name, created_at, updated_at)
    VALUES
        (1, 'user1@company.com', md5('test01'), 'Francisco ','Herrera', NOW(), null),
        (2, 'user2@company.com', md5('test02'), 'Juaquin', 'Montaño', NOW(), null),
        (3, 'user3@company.com', md5('test03'), 'Erick', 'Reyes', NOW(), null),
        (4, 'user4@company.com', md5('test04'), 'Melecio', 'Molina', NOW(), null);
EOS;

$stmInvoice = <<<EOS
    CREATE TABLE IF NOT EXISTS invoices (
        id INT NOT NULL AUTO_INCREMENT,
        client_id INT DEFAULT NULL,
        user_id INT DEFAULT NULL,
        payment_method_id INT DEFAULT NULL,
        note TINYTEXT,
        created_at TIMESTAMP NOT NULL DEFAULT CURRENT_DATE(),
        PRIMARY KEY (id),
        FOREIGN KEY (client_id)
            REFERENCES clients(id)
            ON DELETE SET NULL,
        FOREIGN KEY (payment_method_id)
            REFERENCES payment_methods(id)
            ON DELETE SET NULL,
        FOREIGN KEY (user_id)
            REFERENCES users(id)
            ON DELETE SET NULL
    ) ENGINE=INNODB;

    INSERT INTO invoices
        (id, client_id, user_id, payment_method_id, note, created_at)
    VALUES
        (1, 1, 1, 1,'', NOW()),
        (2, 1, 1, 2,'', NOW()),
        (3, 2, 3, 1,'', NOW()),
        (4, 2, 3, 1,'', NOW()),
        (5, 2, 3, 1,'', NOW()),
        (6, 3, 4, 3,'', NOW());
EOS;

$stmInvoiceArticle = <<<EOS
    CREATE TABLE IF NOT EXISTS invoice_article (
        id INT NOT NULL AUTO_INCREMENT,
        invoice_id INT DEFAULT NULL,
        article_id INT DEFAULT NULL,
        amount INT(5) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (article_id)
            REFERENCES articles(id)
            ON DELETE SET NULL,
        FOREIGN KEY (invoice_id)
            REFERENCES invoices(id)
            ON DELETE SET NULL
    ) ENGINE=INNODB;

    INSERT INTO invoice_article
        (invoice_id, article_id, amount)
    VALUES
        (1, 1, 2),
        (1, 2, 4),
        (1, 4, 1),
        (1, 5, 3),
        (2, 6, 2),
        (2, 7, 1),
        (3, 8, 1),
        (4, 1, 8),
        (4, 2, 2),
        (4, 3, 3),
        (4, 4, 5),
        (5, 5, 2),
        (6, 4, 8);
EOS;

try {
    $dbConnection->exec($stmArticle);
    $dbConnection->exec($stmClient);
    $dbConnection->exec($stmPayment);
    $dbConnection->exec($stmUsers);
    $dbConnection->exec($stmInvoice);
    $dbConnection->exec($stmInvoiceArticle);
    echo "Success!\n";
} catch (\PDOException $e) {
    exit($e->getMessage());
}