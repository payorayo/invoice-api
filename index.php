<?php
require './bootstrap.php';
use App\Core\Router;
use App\Core\Response;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

try{
    $response = new Response();
    $router = new Router($dbConnection, $response, $_SERVER);
    $controller = $router->getController();
    $result = $controller->processRequest();
    header($result['code']);
    echo $response->parseToJson($result);
}
catch (Exception $e){
    http_response_code($e->getCode());
    echo $response->parseToJson(['body' => [], 'message' =>$e->getMessage()]);
}