<?php
use PHPUnit\Framework\TestCase;
use \App\Provider\ArticleProvider;
use \App\Provider\ClientProvider;
use \App\Provider\InvoiceProvider;
use \App\Provider\InvoiceArticleProvider;
use \App\Provider\PaymentMethodProvider;
use \App\Provider\UserProvider;
use \App\Core\Response;
use \App\Controller\InvoiceController;

class InvoiceControllerTest extends TestCase
{
    public function testGetInvoiceWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => ['id' => 1, 'article' => 'value'],
            'message' => 'OK'
        ];

        $provider = $this->createMock(InvoiceProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue(['value']));
        $providerWithArticle = $this->createMock(InvoiceArticleProvider::class);
        $providerWithArticle->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue('value'));

        $providerArticle = $this->createMock(ArticleProvider::class);
        $providerClient = $this->createMock(ClientProvider::class);
        $providerPayment = $this->createMock(PaymentMethodProvider::class);
        $providerUser = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => ['id' => 1, 'article' => 'value'],
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = ['id' => 1];
        $controller = new InvoiceController($provider, $providerWithArticle, $providerClient, $providerPayment, $providerUser, $providerArticle ,$response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testGetAllInvoiceByUserWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(InvoiceProvider::class);
        $provider->expects($this->once())
            ->method('findAllByUser')
            ->will($this->returnValue(['value']));
        $providerWithArticle = $this->createMock(InvoiceArticleProvider::class);
        $providerArticle = $this->createMock(ArticleProvider::class);
        $providerClient = $this->createMock(ClientProvider::class);
        $providerPayment = $this->createMock(PaymentMethodProvider::class);
        $providerUser = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = ['user_id' => 1];
        $controller = new InvoiceController($provider, $providerWithArticle, $providerClient, $providerPayment, $providerUser, $providerArticle ,$response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testInvalidMethodInvoice()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(InvoiceProvider::class);
        $providerWithArticle = $this->createMock(InvoiceArticleProvider::class);
        $providerArticle = $this->createMock(ArticleProvider::class);
        $providerClient = $this->createMock(ClientProvider::class);
        $providerPayment = $this->createMock(PaymentMethodProvider::class);
        $providerUser = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));

        $method = 'DELETE';
        $queryParams = [];
        $controller = new InvoiceController($provider, $providerWithArticle, $providerClient, $providerPayment, $providerUser, $providerArticle ,$response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testCreateInvoice()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(InvoiceProvider::class);
        $provider->expects($this->once())
            ->method('insert')
            ->will($this->returnValue([]));
        $providerWithArticle = $this->createMock(InvoiceArticleProvider::class);
        $providerWithArticle->expects($this->any())
            ->method('insert')
            ->will($this->returnValue([]));
        $providerArticle = $this->createMock(ArticleProvider::class);
        $providerArticle->expects($this->once())
            ->method('findByMultipleIds')
            ->will($this->returnValue(3));
        $providerClient = $this->createMock(ClientProvider::class);
        $providerClient->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $providerPayment = $this->createMock(PaymentMethodProvider::class);
        $providerPayment->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $providerUser = $this->createMock(UserProvider::class);
        $providerUser->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'client_id' => 1,
                'user_id' => 1,
                'payment_method_id' => 1,
                'articles' => [
                    [
                        "article_id" => 9,
                        "amount" => 5
                    ],
                    [
                        "article_id" => 9,
                        "amount" => '4a'
                    ],
                    [
                        "article_id" => 4,
                        "amount" => 4
                    ]
                ]
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'POST';
        $queryParams = [];
        $controller = new InvoiceController($provider, $providerWithArticle, $providerClient, $providerPayment, $providerUser, $providerArticle ,$response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testValidateCreateInvoice()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(InvoiceProvider::class);
        $providerWithArticle = $this->createMock(InvoiceArticleProvider::class);
        $providerArticle = $this->createMock(ArticleProvider::class);
        $providerClient = $this->createMock(ClientProvider::class);
        $providerPayment = $this->createMock(PaymentMethodProvider::class);
        $providerUser = $this->createMock(UserProvider::class);
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'client_id' => 1,
                'user_id' => 1,
                'payment_method_id' => 1,
                'articles' => [
                    [
                        "amount" => 5
                    ],
                    [
                        "article_id" => 9,
                        "amount" => 'F'
                    ],
                    [
                        "article_id" => 4,
                        "amount" => 4
                    ]
                ]
            ]));
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $method = 'POST';
        $queryParams = [];
        $controller = new InvoiceController($provider, $providerWithArticle, $providerClient, $providerPayment, $providerUser, $providerArticle ,$response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }
}