<?php
use PHPUnit\Framework\TestCase;
use \App\Provider\UserProvider;
use \App\Core\Response;
use \App\Controller\AuthController;

class AuthControllerTest extends TestCase {

    public function testLoginWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('login')
            ->will($this->returnValue('value'));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = [
            "email" => "example@test.com",
            "password" => "113"
        ];

        $controller = new AuthController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testMethodInvalidAuth()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));
        $method = 'POST';
        $queryParams = [];

        $controller = new AuthController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateLogin()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $method = 'GET';
        $queryParams = [
            "email" => "example@test.com"
        ];

        $controller = new AuthController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

}