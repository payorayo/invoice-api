<?php

use PHPUnit\Framework\TestCase;
use \App\Provider\PaymentMethodProvider;
use \App\Core\Response;
use \App\Controller\PaymentMethodController;

class PaymentMethodControllerTest extends TestCase
{

    public function testGetAllPaymentMethodsWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(PaymentMethodProvider::class);
        $provider->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue('value'));

        $method = 'GET';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $controller = new PaymentMethodController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testMethodInvalidPaymentMethod()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(PaymentMethodProvider::class);

        $method = 'PUT';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));

        $controller = new PaymentMethodController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testCreatePaymentMethod()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(PaymentMethodProvider::class);
        $provider->expects($this->once())
            ->method('insert')
            ->will($this->returnValue([]));

        $method = 'POST';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa',
                'description' => 'aaaa',
                'amount' => 10,
                'price' => 10
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $controller = new PaymentMethodController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenCreatePaymentMethod()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(PaymentMethodProvider::class);
        $method = 'POST';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => ''
            ]));
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $controller = new PaymentMethodController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }
}