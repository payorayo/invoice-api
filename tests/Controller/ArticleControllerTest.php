<?php
use PHPUnit\Framework\TestCase;
use \App\Provider\ArticleProvider;
use \App\Core\Response;
use \App\Controller\ArticleController;

class ArticleControllerTest extends TestCase {

    public function testGetAllArticlesWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(ArticleProvider::class);
        $provider->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue('value'));

        $method = 'GET';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $controller = new ArticleController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testMethodInvalidArticle()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(ArticleProvider::class);

        $method = 'PUT';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));

        $controller = new ArticleController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testCreateArticles()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(ArticleProvider::class);
        $provider->expects($this->once())
            ->method('insert')
            ->will($this->returnValue([]));

        $method = 'POST';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa',
                'description' => 'aaaa',
                'amount' => 10,
                'price' => 10
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $controller = new ArticleController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenCreateArticles()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(ArticleProvider::class);
        $method = 'POST';
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa',
                'price' => 10
            ]));
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $controller = new ArticleController($provider, $response, $method);
        $this->assertSame($expected, $controller->processRequest());
    }
}