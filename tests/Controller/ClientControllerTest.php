<?php


use PHPUnit\Framework\TestCase;
use \App\Provider\ClientProvider;
use \App\Core\Response;
use \App\Controller\ClientController;

class ClientControllerTest extends TestCase
{

    public function testGetAllClientsWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue('value'));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = [];

        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testGetClientsWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = ['id' => 1];

        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }


    public function testMethodInvalidClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(ClientProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));

        $method = 'OPTION';
        $queryParams = [];
        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenGetClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 404 Not Found',
            'body' => [],
            'message' => 'Not Found'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('notFoundResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 404 Not Found',
                'body' => [],
                'message' => 'Not Found'
            ]));

        $method = 'GET';
        $queryParams = ['id' => 1];
        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testCreateClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('insert')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa',
                'rfc' => 'aaaa',
                'email' => 'test@test.com',
                'address' => '123'
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'POST';
        $controller = new ClientController($provider, $response, $method, []);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenCreateClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa'
            ]));
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $method = 'POST';
        $controller = new ClientController($provider, $response, $method, []);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testUpdateClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $provider->expects($this->once())
            ->method('update')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa',
                'rfc' => 'aaaa',
                'email' => 'test@test.com',
                'address' => '123'
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'PUT';
        $queryParams = ['id' => 1];
        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testDeleteClient()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(ClientProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $provider->expects($this->once())
            ->method('delete')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'DELETE';
        $queryParams = ['id' => 1];
        $controller = new ClientController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }
}