<?php

use PHPUnit\Framework\TestCase;
use \App\Provider\UserProvider;
use \App\Core\Response;
use \App\Controller\UserController;

class UserControllerTest extends TestCase {

    public function testGetAllUsersWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue('value'));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = [];

        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testGetUsersWithMethodGet()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => 'value',
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => 'value',
                'message' => 'OK'
            ]));

        $method = 'GET';
        $queryParams = ['id' => 1];

        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }


    public function testMethodInvalidUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 405 Method Not Allowed',
            'body' => '',
            'message' => 'Method Not Allowed'
        ];

        $provider = $this->createMock(UserProvider::class);

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('methodNotAllowedResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 405 Method Not Allowed',
                'body' => '',
                'message' => 'Method Not Allowed'
            ]));

        $method = 'OPTION';
        $queryParams = [];
        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenGetUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 404 Not Found',
            'body' => [],
            'message' => 'Not Found'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('notFoundResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 404 Not Found',
                'body' => [],
                'message' => 'Not Found'
            ]));

        $method = 'GET';
        $queryParams = ['id' => 1];
        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testCreateUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('findByEmail')
            ->will($this->returnValue([]));
        $provider->expects($this->once())
            ->method('insert')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'first_name' => 'aaaa',
                'last_name' => 'aaaa',
                'email' => 'test@test.com',
                'password' => '123'
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'POST';
        $controller = new UserController($provider, $response, $method, []);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testErrorInValidateWhenCreateUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 422 Unprocessable Entity',
            'body' => [],
            'message' => 'Unprocessable Entity'
        ];

        $provider = $this->createMock(UserProvider::class);
        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'name' => 'aaaa'
            ]));
        $response->expects($this->once())
            ->method('unprocessableEntityResponse')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 422 Unprocessable Entity',
                'body' => [],
                'message' => 'Unprocessable Entity'
            ]));

        $method = 'POST';
        $controller = new UserController($provider, $response, $method, []);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testUpdateUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $provider->expects($this->once())
            ->method('findByEmail')
            ->will($this->returnValue([]));
        $provider->expects($this->once())
            ->method('update')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('getInputs')
            ->will($this->returnValue([
                'first_name' => 'aaaa',
                'last_name' => 'aaaa',
                'email' => 'test@test.com',
                'password' => '123'
            ]));
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'PUT';
        $queryParams = ['id' => 1];
        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }

    public function testDeleteUser()
    {
        $expected = [
            'code' => 'HTTP/1.1 200',
            'body' => [],
            'message' => 'OK'
        ];

        $provider = $this->createMock(UserProvider::class);
        $provider->expects($this->once())
            ->method('find')
            ->will($this->returnValue('value'));
        $provider->expects($this->once())
            ->method('delete')
            ->will($this->returnValue([]));

        $response = $this->createMock(Response::class);
        $response->expects($this->once())
            ->method('show')
            ->will($this->returnValue([
                'code' => 'HTTP/1.1 200',
                'body' => [],
                'message' => 'OK'
            ]));

        $method = 'DELETE';
        $queryParams = ['id' => 1];
        $controller = new UserController($provider, $response, $method, $queryParams);
        $this->assertSame($expected, $controller->processRequest());
    }
}