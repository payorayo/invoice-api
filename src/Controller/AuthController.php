<?php

namespace App\Controller;
use App\Core\Constants;

class AuthController implements IController
{
    private $method;
    private $queryParams;
    private $provider;
    private $response;

    public function __construct($provider, $response, $method, $queryParams)
    {
        $this->method = $method;
        $this->queryParams = $queryParams;
        $this->response = $response;
        $this->provider = $provider;
    }

    public function processRequest()
    {
        switch ($this->method){
            case Constants::GET_METHOD:
                $response = $this->getAuth();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }
        return $response;
    }

    private function getAuth()
    {
        if (! $this->validate($this->queryParams)) {
            return $this->response->unprocessableEntityResponse();
        }

        $result = $this->provider->login($this->queryParams);
        if (! $result) {
            return $this->response->notFoundResponse();
        }
        return $this->response->show($result);
    }


    private function validate($input)
    {
        if (! isset($input['email']) || !filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        if (! isset($input['password'])) {
            return false;
        }
        return true;
    }
}