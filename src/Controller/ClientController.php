<?php

namespace App\Controller;
use App\Core\Constants;

class ClientController implements IController
{
    private $method;
    private $queryParams;
    private $response;
    private $provider;

    public function __construct($provider, $response, $method, $queryParams)
    {
        $this->method = $method;
        $this->queryParams = $queryParams;
        $this->response = $response;
        $this->provider = $provider;
    }

    public function processRequest()
    {
        switch ($this->method){
            case Constants::GET_METHOD:
                if (isset($this->queryParams['id'])) {
                    $response = $this->getClient($this->queryParams['id']);
                } else {
                    $response = $this->getAllClients();
                };
                break;
            case Constants::POST_METHOD:
                $response = $this->createClientFromRequest();
                break;
            case Constants::PUT_METHOD:
                $response = $this->updateClientFromRequest();
                break;
            case Constants::DELETE_METHOD:
                $response = $this->deleteClient();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }
        return $response;
    }

    private function getAllClients()
    {
        $result = $this->provider->findAll();
        return $this->response->show($result);
    }

    private function getClient($id)
    {
        $result = $this->provider->find($id);
        if (! $result) {
            return $this->response->notFoundResponse();
        }
        return $this->response->show($result);
    }

    private function createClientFromRequest()
    {
        $input = $this->response->getInputs();
        if (! $this->validate($input)) {
            return $this->response->unprocessableEntityResponse();
        }
        $this->provider->insert($input);
        return $this->response->show();
    }

    private function updateClientFromRequest()
    {
        $input = $this->response->getInputs();
        if(!isset($this->queryParams['id']) || !$this->validate($input)){
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->find($this->queryParams['id']);
        if (! $result) {
            return $this->response->notFoundResponse();
        }

        $this->provider->update($this->queryParams['id'], $input);
        return $this->response->show();
    }

    private function deleteClient()
    {
        if(!isset($this->queryParams['id'])){
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->find($this->queryParams['id']);
        if (!$result) {
            return $this->response->notFoundResponse();
        }
        $this->provider->delete($this->queryParams['id']);
        return $this->response->show();
    }

    private function validate($input)
    {
        if (! isset($input['name'])) {
            return false;
        }
        if (! isset($input['rfc'])) {
            return false;
        }
        if (! isset($input['email']) || !filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        if (! isset($input['address'])) {
            return false;
        }
        return true;
    }
}