<?php


namespace App\Controller;
use App\Core\Constants;

class ArticleController implements IController
{
    private $method;
    private $provider;
    private $response;

    public function __construct($provider, $response, $method)
    {
        $this->method = $method;
        $this->response = $response;
        $this->provider = $provider;
    }

    public function processRequest(){
        switch ($this->method){
            case Constants::GET_METHOD:
                $response = $this->getAllArticles();
                break;

            case Constants::POST_METHOD:
                $response = $this->createArticleFromRequest();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }
        return $response;
    }

    private function getAllArticles()
    {
        $result = $this->provider->findAll();
        return $this->response->show($result);
    }

    private function createArticleFromRequest()
    {
        $input = $this->response->getInputs();
        if (! $this->validate($input)) {
            return $this->response->unprocessableEntityResponse();
        }

        $this->provider->insert($input);
        return $this->response->show();
    }

    private function validate($input)
    {
        if (! isset($input['name']) || empty($input['name'])) {
            return false;
        }
        if (! isset($input['description']) || empty($input['description'])) {
            return false;
        }
        if (! isset($input['amount']) || !is_numeric($input['amount']) || $input['amount'] <= 0) {
            return false;
        }
        if (! isset($input['price']) || (float) $input['price'] <= 0) {
            return false;
        }
        return true;
    }
}
