<?php

namespace App\Controller;


interface IController
{
    public function processRequest();
}