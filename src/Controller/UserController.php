<?php

namespace App\Controller;
use App\Core\Constants;

class UserController implements IController
{
    private $method;
    private $db;
    private $queryParams;
    private $response;

    private $provider;

    public function __construct($provider, $response, $method, $queryParams)
    {
        $this->method = $method;
        $this->queryParams = $queryParams;
        $this->response = $response;
        $this->provider = $provider;
    }


    public function processRequest()
    {
        switch ($this->method){
            case Constants::GET_METHOD:
                if (isset($this->queryParams['id'])) {
                    $response = $this->getUser($this->queryParams['id']);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            case Constants::POST_METHOD:
                $response = $this->createUserFromRequest();
                break;
            case Constants::PUT_METHOD:
                $response = $this->updateUserFromRequest();
                break;
            case Constants::DELETE_METHOD:
                $response = $this->deleteUser();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }
        return $response;
    }

    private function getAllUsers()
    {
        $result = $this->provider->findAll();
        return $this->response->show($result);
    }

    private function getUser($id)
    {
        $result = $this->provider->find($id);
        if (! $result) {
            return $this->response->notFoundResponse();
        }
        return $this->response->show($result);
    }

    private function createUserFromRequest()
    {
        $input = $this->response->getInputs();
        if (! $this->validate($input)) {
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->findByEmail($input['email']);
        if($result){
            return $this->response->conflictResponse('The email Is Already Exists');
        }
        $this->provider->insert($input);
        return $this->response->show();
    }

    private function updateUserFromRequest()
    {
        $input = $this->response->getInputs();
        if(!isset($this->queryParams['id']) || !$this->validate($input)){
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->find($this->queryParams['id']);
        if (! $result) {
            return $this->response->notFoundResponse();
        }

        $result = $this->provider->findByEmail($input['email']);
        if(empty($result) || count($result) === 1 && $result[0]['id'] === $this->queryParams['id']){
            $this->provider->update($this->queryParams['id'], $input);
            return $this->response->show();
        }
        return $this->response->conflictResponse('The email Is Already Exists');
    }

    private function deleteUser()
    {
        if(!isset($this->queryParams['id'])){
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->find($this->queryParams['id']);
        if (! $result) {
            return $this->response->notFoundResponse();
        }
        $this->provider->delete($this->queryParams['id']);
        return $this->response->show();
    }

    private function validate($input)
    {
        if (! isset($input['first_name'])) {
            return false;
        }
        if (! isset($input['last_name'])) {
            return false;
        }
        if (! isset($input['email']) || !filter_var($input['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        if (! isset($input['password'])) {
            return false;
        }
        return true;
    }
}