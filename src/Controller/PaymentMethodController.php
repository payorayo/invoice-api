<?php

namespace App\Controller;
use App\Core\Constants;

class PaymentMethodController implements IController
{
    private $method;
    private $provider;
    private $response;

    public function __construct($provider, $response, $method)
    {
        $this->method = $method;
        $this->response = $response;
        $this->provider = $provider;
    }

    public function processRequest()
    {
        switch ($this->method){
            case Constants::GET_METHOD:
                $response = $this->getAllPaymentMethods();
                break;

            case Constants::POST_METHOD:
                $response = $this->createPaymentMethodFromRequest();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }

        return $response;
    }

    private function getAllPaymentMethods()
    {
        $result = $this->provider->findAll();
        return $this->response->show($result);
    }

    private function createPaymentMethodFromRequest()
    {
        $input = $this->response->getInputs();
        if (! $this->validate($input)) {
            return $this->response->unprocessableEntityResponse();
        }

        $this->provider->insert($input);
        return $this->response->show();
    }

    private function validate($input)
    {
        if (! isset($input['name']) || empty($input['name'])) {
            return false;
        }
        return true;
    }
}