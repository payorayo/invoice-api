<?php


namespace App\Controller;
use App\Core\Constants;

class InvoiceController implements IController
{
    private $method;
    private $queryParams;
    private $response;

    private $provider;
    private $providerWithArticle;
    private $providerClient;
    private $providerPayment;
    private $providerUser;
    private $providerArticle;

    public function __construct($provider,
        $providerWithArticle,
        $providerClient,
        $providerPayment,
        $providerUser,
        $providerArticle,
        $response,
        $method,
        $queryParams)
    {
        $this->method = $method;
        $this->queryParams = $queryParams;
        $this->response = $response;
        $this->provider = $provider;
        $this->providerWithArticle = $providerWithArticle;
        $this->providerClient = $providerClient;
        $this->providerPayment = $providerPayment;
        $this->providerUser = $providerUser;
        $this->providerArticle = $providerArticle;
    }

    public function processRequest(){
        switch ($this->method){
            case Constants::GET_METHOD:
                if (isset($this->queryParams['id'])) {
                    $response = $this->getInvoice($this->queryParams['id']);
                }
                else {
                    $response = $this->getAllInvoicesByUser();
                }
                break;
            case Constants::POST_METHOD:
                $response = $this->createInvoiceFromRequest();
                break;
            default:
                $response = $this->response->methodNotAllowedResponse();
                break;
        }
        return $response;
    }

    private function getInvoice($id)
    {
        $result = $this->provider->find($id);
        if (! $result) {
            return $this->response->notFoundResponse();
        }
        $result['articles'] = $this->providerWithArticle->findAll($id);
        return $this->response->show($result);
     }

    private function getAllInvoicesByUser()
    {
        if(! isset($this->queryParams['user_id'])){
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->provider->findAllByUser($this->queryParams['user_id']);
        return $this->response->show($result);
    }

    private function createInvoiceFromRequest()
    {
        $input = $this->response->getInputs();
        if (! $this->validate($input)) {
            return $this->response->unprocessableEntityResponse();
        }
        $result = $this->providerClient->find($input['client_id']);
        if(!$result){
            return $this->response->conflictResponse('The client_id Does not Exist');
        }
        $result = $this->providerUser->find($input['user_id']);
        if(!$result){
            return $this->response->conflictResponse('The user_id Does not Exist');
        }
        $result = $this->providerPayment->find($input['payment_method_id']);
        if(!$result){
            return $this->response->conflictResponse('The payment_method_id Does not Exist');
        }
        $result = $this->providerArticle->findByMultipleIds($this->getArticleIds($input['articles']));
        if($result !== count($input['articles'])){
            return $this->response->conflictResponse('Some article_id does not exist');
        }
        $newId = $this->provider->insert($input);
        foreach($input['articles'] as $article){
            $this->providerWithArticle->insert($newId, $article);
        }
        return $this->response->show();
    }

    private function getArticleIds($articles)
    {
        return array_column($articles, 'article_id');
    }

    private function validate($input)
    {
        if (! isset($input['client_id'])) {
            return false;
        }
        if (! isset($input['user_id'])) {
            return false;
        }
        if (! isset($input['payment_method_id'])) {
            return false;
        }
        if (! isset($input['articles']) || empty($input['articles']) || !$this->validateArticles($input['articles'])) {
            return false;
        }
        return true;
    }

    private function validateArticles($articles){
        foreach($articles as $article){
            if(! isset($article['article_id'])){
                return false;
            }
            if(! isset($article['amount']) || $article['amount'] <= 0){
                return false;
            }
        }
        return true;
    }
}