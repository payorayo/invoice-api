<?php

namespace App\Provider;


class InvoiceArticleProvider
{
    private $db= null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function findAll($id)
    {
        $statement = "SELECT article_id, amount FROM invoice_article where invoice_id =?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert($id, Array $input)
    {
        $statement = "INSERT INTO invoice_article
        (invoice_id, article_id, amount)
        VALUES (:invoice_id, :article_id, :amount);";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'invoice_id' => $id,
                'article_id'  => $input['article_id'],
                'amount' => (int) $input['amount']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}