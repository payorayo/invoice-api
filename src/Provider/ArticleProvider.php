<?php

namespace App\Provider;


class ArticleProvider
{
    private $db= null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function find($id)
    {
        $statement = "SELECT id, name, description, amount, price FROM articles WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findByMultipleIds($ids)
    {
        $total = 0;
        foreach($ids as $id){
            $res = $this->find($id);
            if($res){
                $total ++;
            }
        }
        return $total;
    }

    public function findAll()
    {
        $statement = "SELECT id, name, description, amount, price FROM articles;";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "INSERT INTO articles (name, description, amount, price, created_at)
                    VALUES (:name, :description, :amount, :price, NOW());";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'name' => $input['name'],
                'description'  => $input['description'],
                'amount' => $input['amount'],
                'price' => $input['price'],
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}