<?php

namespace App\Provider;


class UserProvider
{
    private $db= null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function login(Array $input)
    {
        $statement = "SELECT id, email, first_name, last_name FROM users 
        WHERE email = :email and password=md5(:password);";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'email' => $input['email'],
                'password' => $input['password']
            ));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findAll()
    {
        $statement = "SELECT id, email, first_name, last_name FROM users;";

        try {
            $statement = $this->db->query($statement);
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function find($id)
    {
        $statement = "SELECT id, email, first_name, last_name FROM users WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findByEmail($email)
    {
        $statement = "SELECT id, email, first_name, last_name FROM users WHERE email = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($email));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }


    public function insert(Array $input)
    {
        $statement = "INSERT INTO users
        (email, password, first_name, last_name, created_at)
        VALUES (:email, md5(:password), :first_name, :last_name, NOW());";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'email' => $input['email'],
                'password' => $input['password'],
                'first_name'  => $input['first_name'],
                'last_name' => $input['last_name']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function update($id, Array $input)
    {
        $statement = "UPDATE users
            SET email  = :email,
                password = md5(:password),
                first_name = :first_name,
                last_name = :last_name,
                updated_at = NOW()
            WHERE id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'id' => (int) $id,
                'email' => $input['email'],
                'password' => $input['password'],
                'first_name'  => $input['first_name'],
                'last_name' => $input['last_name']
            ));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function delete($id)
    {
        $statement = "DELETE FROM users where id = :id;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array('id' => $id));
            return $statement->rowCount();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}