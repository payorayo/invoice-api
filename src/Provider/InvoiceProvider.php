<?php


namespace App\Provider;


class InvoiceProvider
{
    private $db= null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function find($id)
    {
        $statement = "SELECT id, client_id, user_id, payment_method_id, note FROM invoices WHERE id = ?;";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function findAllByUser($id)
    {
        $statement = "SELECT i.id, c.`name` as clientname, u.email as username, p.name as payment, i.note, sum(ia.amount) as articles
            FROM invoices as i
            INNER JOIN clients as c on c.id = i.client_id
            INNER JOIN users as u on u.id = i.user_id
            INNER JOIN payment_methods as p on p.id = i.payment_method_id
            INNER JOIN invoice_article as ia on ia.invoice_id = i.id
            WHERE i.user_id = ?
            GROUP BY i.id
            ORDER BY i.created_at DESC";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array($id));
            $result = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function insert(Array $input)
    {
        $statement = "INSERT INTO invoices
        (client_id, user_id, payment_method_id, note, created_at)
        VALUES (:client_id, :user_id, :payment_method_id, :note, NOW());";

        try {
            $statement = $this->db->prepare($statement);
            $statement->execute(array(
                'client_id' => (int) $input['client_id'],
                'user_id'  => (int) $input['user_id'],
                'payment_method_id' => (int) $input['payment_method_id'],
                'note' => $input['note']
            ));
            return $this->db->lastInsertId();
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }
}