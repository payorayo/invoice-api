<?php

namespace App\Core;
require_once __DIR__.'/../../config/core.php';
use Exception;
use App\Controller\ArticleController;
use App\Controller\AuthController;
use App\Controller\ClientController;
use App\Controller\InvoiceController;
use App\Controller\PaymentMethodController;
use App\Controller\UserController;
use App\Provider\InvoiceProvider;
use App\Provider\InvoiceArticleProvider;
use App\Provider\ClientProvider;
use App\Provider\PaymentMethodProvider;
use App\Provider\UserProvider;
use App\Provider\ArticleProvider;

class Router
{
    private $server;
    private $db;
    private $response;
    public function __construct(
        $db,
        $response,
        array $server = []
    ) {
        $this->db = $db;
        $this->response = $response;
        $this->server = $server;
    }

    public function getController()
    {
        $uri = parse_url($this->server['REQUEST_URI'], PHP_URL_PATH);
        $name = str_replace(FOLDER_ROOT, '', $uri);
        $method = $this->getMethod();
        $query = $this->getQueryParams();

        $articleProvider = new ArticleProvider($this->db);
        $userProvider = new UserProvider($this->db);
        $clientProvider = new ClientProvider($this->db);
        $paymentProvider = new PaymentMethodProvider($this->db);
        $invoiceProvider = new InvoiceProvider($this->db);
        $invoiceArticleProvider = new InvoiceArticleProvider($this->db);

        switch (strtolower($name)){
            case constants::ARTICLE_ACTION:
                return new ArticleController($articleProvider, $this->response, $method);
            case constants::AUTH_ACTION:
                return new AuthController($userProvider, $this->response, $method, $query);
            case constants::CLIENT_ACTION:
                return new ClientController($clientProvider, $this->response, $method, $query);
            case constants::INVOICE_ACTION:
                return new InvoiceController(
                    $invoiceProvider,
                    $invoiceArticleProvider,
                    $clientProvider,
                    $paymentProvider,
                    $userProvider,
                    $articleProvider,
                    $this->response,
                    $method,
                    $query
                );
            case constants::PAYMENT_ACTION:
                return new PaymentMethodController($paymentProvider, $this->response, $method);
            case constants::USER_ACTION:
                return new UserController($userProvider, $this->response, $method, $query);
            default:
                throw new Exception(constants::HTTP_MESSAGE_NOT_FOUND_PATH.$name, 405);
        }
    }

    private function getMethod()
    {
        return $this->server["REQUEST_METHOD"];
    }

    private function getQueryParams(){
        $query = parse_url($this->server['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($query, $params);

        return $params;
    }
}