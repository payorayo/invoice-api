<?php

namespace App\Core;
require_once __DIR__.'/../../config/core.php';

class Connector
{
    private $dbConnection = null;

    public function __construct()
    {
        try {
            $this->dbConnection = new \PDO(
                "mysql:host=".DB_HOST.";port=".DB_PORT.";charset=utf8;dbname=".DB_NAME,
                DB_USER,
                DB_PASS
            );
        } catch (\PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function getConnection()
    {
        return $this->dbConnection;
    }
}