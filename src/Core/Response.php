<?php

namespace App\Core;

class Response
{
    public function unprocessableEntityResponse()
    {
        return $this->show('', Constants::HTTP_HEADER_422, Constants::HTTP_MESSAGE_422);
    }

    public function conflictResponse($msg)
    {
        return $this->show('', Constants::HTTP_HEADER_409, $msg);
    }

    public function notFoundResponse()
    {
        return $this->show('', Constants::HTTP_HEADER_404, Constants::HTTP_MESSAGE_404);
    }

    public function methodNotAllowedResponse($message = Constants::HTTP_MESSAGE_405)
    {
        return $this->show('', Constants::HTTP_HEADER_405, $message);
    }

    public function show($body= [], $code = Constants::HTTP_HEADER_200, $message = Constants::HTTP_MESSAGE_200){
        return [
            'code' => $code,
            'body' => $body,
            'message' => $message
        ];
    }

    public function parseToJson($response){
        unset($response['code']);
        return json_encode($response);
    }

    public function getInputs(){
        return json_decode(file_get_contents('php://input'), TRUE);
    }
}