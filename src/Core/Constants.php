<?php


namespace App\Core;


class Constants
{
    const HTTP_HEADER_422 = 'HTTP/1.1 422 Unprocessable Entity';
    const HTTP_HEADER_409 = 'HTTP/1.1 409 Conflict';
    const HTTP_HEADER_404 = 'HTTP/1.1 404';
    const HTTP_HEADER_405 = 'HTTP/1.1 405 Method Not Allowed';
    const HTTP_HEADER_200 = '200';

    const HTTP_MESSAGE_422 = 'Invalid input';
    const HTTP_MESSAGE_404 = 'Not Found';
    const HTTP_MESSAGE_405 = 'Method Not Allowed';
    const HTTP_MESSAGE_200 = 'OK';
    const HTTP_MESSAGE_NOT_FOUND_PATH = 'PATH cannot be found: ';

    CONST GET_METHOD = 'GET';
    CONST POST_METHOD = 'POST';
    CONST PUT_METHOD = 'PUT';
    CONST DELETE_METHOD = 'DELETE';

    const CLIENT_ACTION = 'client';
    const AUTH_ACTION = 'auth';
    const ARTICLE_ACTION = 'article';
    const INVOICE_ACTION = 'invoice';
    const PAYMENT_ACTION = 'paymentmethod';
    const USER_ACTION = 'user';
}